import { BsCaretRight } from "react-icons/bs";
import CardTwoListV from "./cardTwo-list";

const CardTwo = () => {

  return (
    <>
      <div className="container">
        <div
          id="tiepost-131-section-3871"
          className="section-wrapper container-full has-background"
        >
          {" "}
          <div
            className="section-item full-width"
            style={{ backgroundColor: "#161616", backgroundSize: "cover" }}
          >
            {" "}
            <div className="container">
              {" "}
              <div className="tie-row main-content-row">
                {" "}
                <div className="main-content col-md-12">
                  {" "}
                  <section
                    id="tie-block_841"
                    className="slider-area mag-box tie-video-main-slider"
                  >
                    {" "}
                    <div className="videos-block" id="videos-block-id-0">
                      {" "}
                      <div className="video-playlist-wrapper">
                        {" "}
                        <div className="video-player-wrapper tie-ignore-fitvid">
                          {" "}
                          <iframe
                            className="video-frame"
                            id="video-block_841-1"
                            src="//player.vimeo.com/video/210672131?api=1&amp;title=0&amp;byline=0&amp;color=00adef&amp;player_id=video-block_841-1"
                            title="Videos List"
                            width="771"
                            height="434"
                            frameborder="0"
                            webkitallowfullscreen=""
                            mozallowfullscreen=""
                            allowfullscreen=""
                            async=""
                            style={{ visibility: "visible" }}
                            data-video-ready=""
                          ></iframe>{" "}
                        </div>{" "}
                      </div>{" "}
                      <div className="video-playlist-nav-wrapper">
                        {" "}
                        <div className="playlist-title">
                          {" "}
                          <div className="playlist-title-icon">
                            <BsCaretRight />
                          </div>{" "}
                          <h2>Most Watched</h2>{" "}
                          <span className="videos-number">
                            {" "}
                            <span className="video-playing-number">
                              1
                            </span> /{" "}
                            <span className="video-totlal-number">7</span>{" "}
                            Videos{" "}
                          </span>{" "}
                        </div>{" "}
                        <div
                          data-height="window"
                          className="video-playlist-nav has-custom-scroll playlist-has-title"
                        >
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                        </div>{" "}
                      </div>{" "}
                    </div>{" "}
                  </section>{" "}
                </div>{" "}
              </div>{" "}
            </div>{" "}
          </div>{" "}
        </div>
      </div>
    </>
  );
};

export default CardTwo;
