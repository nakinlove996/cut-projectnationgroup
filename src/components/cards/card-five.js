import { BsCaretRight } from "react-icons/bs";
import CardTwoListV from "./cardTwo-list";

const CardFive = () => {
  return (
    <>
      <div className="container">
        <div
          id="tiepost-131-section-3871"
          className="section-wrapper container-full has-background"
        >
          {" "}
          <div
            className="section-item full-width has-video-background tie-parallax"
            style={{ backgroundColor: "#161616" }}
            data-jarallax-video="https://youtu.be/bYfp13VoGuQ?list=PLj6XzcqwRpN4Ha7s3xr1Eo4EKcv7SWEkY"
            data-type="scroll"
          >
            {" "}
            <div className="container">
              {" "}
              <div className="tie-row main-content-row">
                {" "}
                <div className="main-content col-md-12">
                  {" "}
                  <section
                    id="tie-block_841"
                    className="slider-area mag-box tie-video-main-slider"
                  >
                    {" "}
                    <div className="videos-block" id="videos-block-id-0">
                      {" "}
                      <div className="video-playlist-wrapper">
                        {" "}
                        <div className="video-player-wrapper tie-ignore-fitvid">
                          {" "}
                          <iframe
                            className="video-frame"
                            id="video-block_841-1"
                            src="//player.vimeo.com/video/206402159?api=1&title=0&byline=0&color=00adef&player_id=video-block_2097-1"
                            title="Videos List"
                            width="771"
                            height="434"
                            frameborder="0"
                            webkitallowfullscreen=""
                            mozallowfullscreen=""
                            allowfullscreen=""
                            async=""
                            style={{ visibility: "visible" }}
                            data-video-ready=""
                          ></iframe>{" "}
                        </div>{" "}
                      </div>{" "}
                      <div className="video-playlist-nav-wrapper">
                        {" "}
                        <div className="playlist-title">
                          {" "}
                          <div className="playlist-title-icon">
                            <BsCaretRight />
                          </div>{" "}
                          <h2>And More</h2>{" "}
                          <span className="videos-number">
                            {" "}
                            <span className="video-playing-number">
                              1
                            </span> /{" "}
                            <span className="video-totlal-number">6</span>{" "}
                            Videos{" "}
                          </span>{" "}
                        </div>{" "}
                        <div
                          data-height="window"
                          className="video-playlist-nav has-custom-scroll playlist-has-title"
                        >
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                          <CardTwoListV />
                        </div>{" "}
                      </div>{" "}
                    </div>{" "}
                  </section>{" "}
                </div>{" "}
              </div>{" "}
            </div>{" "}
            <div
              id="jarallax-container-0"
              style={{
                position: "absolute",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
                overflow: "hidden",
                pointerEvents: "none",
                zIndex: "-100",
              }}
            >
              <img
                src="https://img.youtube.com/vi/bYfp13VoGuQ/maxresdefault.jpg"
                style={{
                  objectFit: "cover",
                  maxWidth: "none",
                  position: "fixed",
                  top: "0px",
                  left: "83.5px",
                  width: "1230px",
                  height: "788.5px",
                  overflow: "hidden",
                  pointerEvents: "none",
                  marginTop: "147.25px",
                  transform: "translateY(-526.938px) translateZ(0px)",
                  display: "none",
                }}
              />
              <iframe
                id="VideoWorker-0"
                frameborder="0"
                allowfullscreen="1"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                title="YouTube video player"
                width="640"
                height="360"
                src="https://www.youtube.com/embed/bYfp13VoGuQ?autohide=1&amp;rel=0&amp;autoplay=0&amp;iv_load_policy=3&amp;modestbranding=1&amp;controls=0&amp;showinfo=0&amp;disablekb=1&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fjannah.tielabs.com&amp;widgetid=1"
                style={{
                  position: "fixed",
                  inset: " 0px 0px 0px 83.5px",
                  width: "1401.78px",
                  height: "1188.5px",
                  maxWidth: 'none',
                  maxHeight: "none",
                  margin: "-52.75px 0px 0px -85.8889px",
                  zIndex: "-1",
                  transform: "translateY(-218.938px) translateZ(0px)",
                }}
              ></iframe>
            </div>
          </div>{" "}
        </div>
      </div>
    </>
  );
};

export default CardFive;
