import { BsCaretRight } from "react-icons/bs";
import { AiFillFire } from "react-icons/ai";
import { AiFillMessage } from "react-icons/ai";
import { AiOutlineUser } from "react-icons/ai";

const CardThree = () => {
  return (
    <>
      <li className="post-item post-4127 post type-post status-publish format-standard has-post-thumbnail category-electronics tag-advice tag-tip tag-tips tag-top tie-video">
        <a aria-label="" href="#" className="post-thumb">
          <span className="post-cat-wrap">
            <span className="post-cat tie-cat-243">
              Electronics
                              </span>
          </span>
          <div className="post-thumb-overlay-wrap">
            {" "}
            <div className="post-thumb-overlay">
              {" "}
              <BsCaretRight className="tie-media-icon" />{" "}
            </div>{" "}
          </div>
          <img
            width="390"
            height="220"
            src="https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/02/maxresdefault-5-390x220.jpg"
            className="attachment-jannah-image-large size-jannah-image-large lazy-img wp-post-image"
            alt=""
            loading="lazy"
          ></img>
        </a>
        <div className="post-details">
          {" "}
          <div className="post-meta clearfix">
            <span className="author-meta single-author no-avatars">
              <span className="meta-item meta-author-wrapper meta-author-1">
                <span className="meta-author">
                  <AiOutlineUser className='icon' />
                                    Tony Stark
                                  </span>
              </span>
            </span>
            <span className="date meta-item tie-icon">
              Feb 6, 2017
                              </span>
            <div className="tie-alignright">
              <span className="meta-comment tie-icon meta-item fa-before">
                <AiFillMessage /> 0
                                </span>
              <span className="meta-views meta-item hot">
                <AiFillFire /> 2,936{" "}
              </span>
            </div>
          </div>{" "}
          <h2 className="post-title">
            <a href="2017/02/06/better-than-robert-redford-dessert/">
              Top 5 – Computer Gadgets You Must Have
                              </a>
          </h2>{" "}
          <p className="post-excerpt">
            Stay focused and remember we design the best
            WordPress News and Magazine Themes. It’s the ones
            closest to you that…
                            </p>{" "}
          <a
            className="more-link btn btn-info " style={{ background: '#00adef' }}
            href="2017/02/06/better-than-robert-redford-dessert/"
          >
            Read More »
                            </a>{" "}
        </div>
      </li>
    </>
  )
}

export default CardThree
