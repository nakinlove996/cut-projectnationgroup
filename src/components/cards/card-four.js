import { BsCaretRight } from "react-icons/bs";

const CardFour = () => {
  return (
    <>
      <li className="widget-single-post-item widget-post-list tie-video">
        <div className="post-widget-thumbnail">
          <a href="#" aria-label="" class="post-thumb">
            <div className="post-thumb-overlay-wrap">
              <div class="post-thumb-overlay-wrap">
                {" "}
                <div class="post-thumb-overlay">
                  {" "}
                  <BsCaretRight className="tie-icon tie-media-icon" />{" "}
                </div>{" "}
              </div>
            </div>
            <img
              src="https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/02/maxresdefault-5-220x150.jpg"
              alt=""
              className="attachment-jannah-image-small size-jannah-image-small lazy-img tie-small-image wp-post-image"
            />
          </a>
        </div>
        <div class="post-widget-body ">
          {" "}
          <a
            class="post-title the-subtitle"
            href="2017/02/06/better-than-robert-redford-dessert/"
          >
            Top 5 – Computer Gadgets You Must Have
          </a>{" "}
          <div class="post-meta">
            {" "}
            <span class="date meta-item tie-icon">Feb 6, 2017</span>{" "}
          </div>{" "}
        </div>
      </li>
    </>
  );
};

export default CardFour;
