const cardTwoListV = () => {
  return (
    <>
      <div
        data-name="video-block_841-1"
        data-video-src="//player.vimeo.com/video/210672131?api=1&amp;title=0&amp;byline=0&amp;color=00adef"
        className="video-playlist-item is-paused"
      >
        {" "}
        <div className="video-number">1</div>{" "}
        <div className="video-play-icon">
          <span className="tie-icon-play" aria-hidden="true"></span>
        </div>{" "}
        <div className="video-paused-icon">
          <span className="tie-icon-pause" aria-hidden="true"></span>
        </div>{" "}
        <img
          className="video-thumbnail post-thumb"
          src="https://i.vimeocdn.com/video/626521740_100x75.jpg"
          alt=""
        />{" "}
        <div className="video-info">
          {" "}
          <h2>Samsung Galaxy S8 and S8 to Telefonia Milano Bronzetti</h2>{" "}
          <span className="video-duration">03:34</span>{" "}
        </div>{" "}
      </div>
    </>
  );
};

export default cardTwoListV;
