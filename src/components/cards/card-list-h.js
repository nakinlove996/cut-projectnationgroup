import { BsCaretRight } from "react-icons/bs";
import { AiFillFire } from "react-icons/ai";
import { AiFillMessage } from "react-icons/ai";
import { AiOutlineUser } from "react-icons/ai";

export const CardListH = () => {
  return (
    <>
      <li class="widget-single-post-item widget-post-list tie-video">
        <div class="post-widget-thumbnail">
          <a
            aria-label="Top 5 – Computer Gadgets You Must Have"
            href="2017/02/06/better-than-robert-redford-dessert/"
            class="post-thumb"
          >
            <span class="post-cat-wrap">
              <span class="post-cat tie-cat-243">Electronics</span>
            </span>
            <div class="post-thumb-overlay-wrap">
              <div class="post-thumb-overlay">
                <span class="tie-icon tie-media-icon"></span>
              </div>
            </div>
            <img
              width="390"
              height="220"
              src="https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/02/maxresdefault-5-390x220.jpg"
              class="attachment-jannah-image-large size-jannah-image-large lazy-img wp-post-image"
              alt=""
              loading="lazy"
              style={{ marginBottom: '10px' }}
            />
          </a>{" "}
        </div>{" "}
        <div class="post-widget-body ">
          {" "}
          <a
            class="post-title the-subtitle"
            href="2017/02/06/better-than-robert-redford-dessert/"
            style={{ fontSize: '18px' }}
          >
            Top 5 – Computer Gadgets You Must Have
          </a>{" "}
          <div class="post-meta">
            {" "}
            <span class="date meta-item tie-icon">Feb 6, 2017</span>{" "}
          </div>{" "}
        </div>{" "}
      </li>
    </>
  );
};
