import { BsCaretRight } from "react-icons/bs";


const CardOverlay = () => {
  return (
    <>
      <div
        style={{ backgroundImage: 'url(https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/02/maxresdefault-780x470.jpg)' }}
        class="grid-item slide-id-4113 tie-slide-1 tie-video"
      >
        <a
          href="2017/02/06/pick-a-topper-appetizer-flatbreads/"
          class="all-over-thumb-link"
          aria-label="Top 9 New Technology Future Inventions Gadgets Coming in 2017"
          tabindex="0"
        ></a>
        <div class="thumb-overlay">
          <BsCaretRight class="tie-icon tie-media-icon" />
          <span class="post-cat-wrap">
            <a
              class="post-cat tie-cat-241"
              href="category/technology/computing/"
              tabindex="0"
            >
              Computing
                                        </a>
          </span>
          <div class="thumb-content">
            <div class="thumb-meta">
              <span class="date meta-item tie-icon">
                Feb 6, 2017
                                          </span>
            </div>
            <h2 class="thumb-title">
              <a
                href="2017/02/06/pick-a-topper-appetizer-flatbreads/"
                tabindex="0"
              >
                Top 9 New Technology Future
                Inventions Gadgets Coming in 2017
                                          </a>
            </h2>
            <div class="thumb-desc">
              Stay focused and remember we design
              the best WordPress News and Magazine
              Themes. It’s the ones closest to you
              that…
                                        </div>
          </div>
        </div>
      </div>

    </>
  );
};

export default CardOverlay;
