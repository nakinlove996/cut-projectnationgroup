export default function ardFanpage() {
  return (
    <>
      <div
        id="youtube-widget-3"
        class="container-wrapper widget widget_youtube-widget"
      >
        <div class="widget-title the-global-title has-block-head-4">
          <div class="the-subtitle">
            Subscribe to our channel
            <span class="widget-title-icon tie-icon"></span>
          </div>
        </div>{" "}
        <div class="youtube-box tie-ignore-fitvid">
          {" "}
          <div
            id="___ytsubscribe_0"
            style={{
              textIndent: "0px",
              margin: "0px",
              padding: "0px",
              background: "transparent",
              borderStyle: "none",
              float: "none",
              lineHeight: "normal",
              fontSize: "1px",
              verticalAlign: "baseline",
              display: "inline-block",
              width: "177px",
              height: "48px",
            }}
          >
            <iframe
              ng-non-bindable=""
              frameborder="0"
              hspace="0"
              marginheight="0"
              marginwidth="0"
              scrolling="no"
              style={{
                position: "static",
                top: "0px",
                width: "177px",
                margin: "0px",
                borderStyle: "none",
                left: "0px",
                visibility: "visible",
                height: "48px",
              }}
              tabindex="0"
              vspace="0"
              width="100%"
              id="I0_1623228813472"
              name="I0_1623228813472"
              src="https://www.youtube.com/subscribe_embed?usegapi=1&amp;channel=unboxtherapy&amp;layout=full&amp;count=default&amp;origin=https%3A%2F%2Fjannah.tielabs.com&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en_US.p7L79FLXQCw.O%2Fam%3DAQ%2Fd%3D1%2Frs%3DAGLTcCO6hl1EejjzC-wrWbDdgTxPi0Gs8g%2Fm%3D__features__#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1623228813472&amp;_gfid=I0_1623228813472&amp;parent=https%3A%2F%2Fjannah.tielabs.com&amp;pfname=&amp;rpctoken=18790458"
              data-gapiattached="true"
            ></iframe>
          </div>{" "}
        </div>{" "}
        <div class="clearfix"></div>
      </div>
    </>
  );
}
