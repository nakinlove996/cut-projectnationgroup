import CardThree from "../../cards/card-three";
import { AiFillFile } from "react-icons/ai";
import CardFour from "../../cards/card-four";
import AdsOne from "../../ads/ads-one";
import { CardListH } from "../../cards/card-list-h";
import { CardSeven } from "../../cards/card-seven";
import CardFanpage from "../../cards/cardFanpage";

import { FaFacebook } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaYoutube } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";

export default function SectionFive() {
  return (
    <>
      <div
        id="tiepost-131-section-1508"
        className="section-wrapper container normal-width without-background"
      >
        <div className="section-item sidebar-right has-sidebar">
          <div className="container-normal">
            <div className="tie-row main-content-row">
              <div className="main-content col-md-8 col-xs-12" role="main">
                <AdsOne />
                <div
                  id="tie-block_3075"
                  className="mag-box big-posts-box media-overlay"
                >
                  <div className="container-wrapper">
                    {/* HeadTitle */}
                    <div className="mag-box-title the-global-title">
                      <h3>Electronics</h3>
                    </div>
                    {/* sectionBody */}
                    <div className="mag-box-container clearfix">
                      <ul className="posts-items posts-list-container">
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                      </ul>
                    </div>
                    {/* Show */}

                    <a
                      className="block-pagination next-posts show-more-button"
                      data-text="Show More"
                      href=""
                    >
                      Show More
                    </a>
                  </div>
                </div>
              </div>
              <aside
                className="sidebar col-md-4 col-xs-12 normal-side is-sticky is-alreay-loaded"
                style={{
                  position: "relative",
                  overflow: "visible",
                  boxSizing: "border-box",
                  minHeight: "1px",
                }}
              >
                <div
                  className="theiaStickySidebar"
                  style={{
                    paddingTop: "0px",
                    paddingBottom: "1px",
                    position: "static",
                    transform: "none",
                    top: "0px",
                    left: "928.016px",
                  }}
                >
                  <div
                    id="posts-list-widget-30"
                    className="container-wrapper widget posts-list"
                  >
                    <div class="widget-title the-global-title has-block-head-4">
                      <div class="the-subtitle">
                        <AiFillFile
                          className="widget-title-icon"
                          style={{ marginRight: "2px", alignItems: "center" }}
                        />
                        Most Popular Videos
                      </div>
                    </div>

                    <div className="widget-posts-list-container posts-list-counter media-overlay">
                      <ul className="posts-list-items widget-posts-wrapper">
                        <CardListH />
                        <CardFour />
                        <CardFour />
                      </ul>
                    </div>
                  </div>
                </div>
              </aside>
              {/* ---------------------------------------------------------------------- */}
              <aside
                className="sidebar col-md-4 col-xs-12 normal-side is-sticky is-alreay-loaded"
                style={{
                  position: "relative",
                  overflow: "visible",
                  boxSizing: "border-box",
                  minHeight: "1px",
                }}
              >
                <div
                  className="theiaStickySidebar"
                  style={{
                    paddingTop: "0px",
                    paddingBottom: "1px",
                    position: "static",
                    transform: "none",
                    top: "0px",
                    left: "928.016px",
                  }}
                >
                  <div
                    id="posts-list-widget-30"
                    className="container-wrapper widget posts-list"
                  >
                    <div class="widget-title the-global-title has-block-head-4">
                      <div class="the-subtitle">
                        <AiFillFile
                          className="widget-title-icon"
                          style={{ marginRight: "2px", alignItems: "center" }}
                        />
                        Don’t Miss
                      </div>
                      <div className="widget-posts-list-container posts-list-half-posts media-overlay">
                        <ul className="posts-list-items widget-posts-wrapper">
                          <CardSeven />
                          <CardSeven />
                          <CardSeven />
                          <CardSeven />
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </aside>

              <aside
                className="sidebar col-md-4 col-xs-12 normal-side is-sticky is-alreay-loaded"
                style={{
                  position: "relative",
                  overflow: "visible",
                  boxSizing: "border-box",
                  minHeight: "1px",
                }}
              >
                <div
                  className="theiaStickySidebar"
                  style={{
                    paddingTop: "0px",
                    paddingBottom: "1px",
                    position: "static",
                    transform: "none",
                    top: "0px",
                    left: "928.016px",
                  }}
                >
                  <CardFanpage />
                </div>

                {/* ---------------------------------------------------------------------- Follow Us */}
                <div
                  id="social-statistics-19"
                  class="container-wrapper widget social-statistics-widget"
                >
                  <div class="widget-title the-global-title has-block-head-4">
                    <div class="the-subtitle">
                      Follow Us<span class="widget-title-icon tie-icon"></span>
                    </div>
                  </div>{" "}
                  <ul class="solid-social-icons solid-social-icons white-bg two-cols circle-icons Arqam-Lite">
                    {" "}
                    <li class="social-icons-item">
                      {" "}
                      <a
                        class="facebook-social-icon"
                        href="//www.facebook.com/tielabs"
                        rel="nofollow noopener"
                        target="_blank"
                      >
                        {" "}
                        <FaFacebook
                          className="counter-icon "
                          style={{ color: "#1877f2" }}
                        />{" "}
                        <span class="followers">
                          {" "}
                          <span class="followers-num">14,802</span>{" "}
                          <span class="followers-name">Fans</span>{" "}
                        </span>{" "}
                      </a>{" "}
                    </li>{" "}
                    <li class="social-icons-item">
                      {" "}
                      <a
                        class="twitter-social-icon"
                        href="//twitter.com/tielabs"
                        rel="nofollow noopener"
                        target="_blank"
                      >
                        {" "}
                        <FaTwitter
                          className="counter-icon "
                          style={{ color: "rgba(29,161,242,1.00)" }}
                        />{" "}
                        <span class="followers">
                          {" "}
                          <span class="followers-num">1,126</span>{" "}
                          <span class="followers-name">Followers</span>{" "}
                        </span>{" "}
                      </a>{" "}
                    </li>{" "}
                    <li class="social-icons-item">
                      {" "}
                      <a
                        class="youtube-social-icon"
                        href="//youtube.com/user/TEAMMESAI"
                        rel="nofollow noopener"
                        target="_blank"
                      >
                        {" "}
                        <FaYoutube
                          className="counter-icon "
                          style={{ color: "#f00" }}
                        />{" "}
                        <span class="followers">
                          {" "}
                          <span class="followers-num">43,700</span>{" "}
                          <span class="followers-name">Followers</span>{" "}
                        </span>{" "}
                      </a>{" "}
                    </li>{" "}
                    <li class="social-icons-item">
                      {" "}
                      <a
                        class="instagram-social-icon"
                        href="//instagram.com/imo3aser"
                        rel="nofollow noopener"
                        target="_blank"
                      >
                        {" "}
                        <FaInstagram
                          className="counter-icon "
                          style={{ color: "#c13584" }}
                        />{" "}
                        <span class="followers">
                          {" "}
                          <span class="followers-num">0</span>{" "}
                          <span class="followers-name">Followers</span>{" "}
                        </span>{" "}
                      </a>{" "}
                    </li>{" "}
                  </ul>{" "}
                  <div class="clearfix"></div>
                </div>
              </aside>

              {/* --------------------------------------------------------------------- */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
