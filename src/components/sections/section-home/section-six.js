import CardThreeBouttom from "../../cards/card-bottom";

export default function SectionSix() {
  return (
    <>
      <div
        id="tiepost-131-section-1508"
        className="section-wrapper container-full without-background"
      >
        <div className="section-item full-width">
          <div className="container">
            <div className="tie-row main-content-row">
              <div className="main-content col-md-12 " >
                <div
                  id="tie-block_2485"
                  className="mag-box big-posts-box media-overlay"
                >
                  <div className="container-wrapper">
                    {/* HeadTitle */}
                    <div className="mag-box-title the-global-title">
                      <h3>Tips & Ideas</h3>
                    </div>
                    {/* sectionBody */}
                    <div className="mag-box-container clearfix">
                      <ul className="posts-items posts-list-container">
                        <CardThreeBouttom />
                        <CardThreeBouttom />
                        <CardThreeBouttom />
                      </ul>
                    </div>
                    {/* Show */}
                    <a
                      className="block-pagination next-posts show-more-button"
                      data-text="Show More"
                      href=""
                    >
                      Load More
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
