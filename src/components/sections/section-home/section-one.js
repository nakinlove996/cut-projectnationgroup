import { useEffect, useState } from 'react'
import CardOverlay from "../../cards/card-overlay";

function debounce(fn, ms) {
  let timer
  return _ => {
    clearTimeout(timer)
    timer = setTimeout(_ => {
      timer = null
      fn.apply(this, arguments)
    }, ms)
  };
}

export default function SectionOne() {
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  })
  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
    }, 1000)

    window.addEventListener('resize', debouncedHandleResize)

    return _ => {
      window.removeEventListener('resize', debouncedHandleResize)

    }
  })
  return (
    <div className="container">
      <div
        class="section-wrapper container-full has-background"
      >
        <div
          class="section-item is-first-section full-width"
          style={{ backgroundColor: 'rgb(22, 22, 22)', backgroundSize: 'cover' }}
        >
          <div class="container">
            <div class="tie-row main-content-row">
              <div class="main-content col-md-12">
                <section
                  class="slider-area mag-box media-overlay"
                >
                  <div class="mag-box-title the-global-title">
                    <h3> Most Recent </h3>
                  </div>
                  <div class="slider-area-inner">
                    <div
                      class="tie-main-slider main-slider grid-6-slides boxed-slider grid-slider-wrapper tie-slick-slider-wrapper"
                      data-slider-id="15"
                      data-speed="3000"
                    >
                      <div class="main-slider-inner">
                        <div class="containerblock_2408">
                          <div class="tie-slick-slider slick-initialized slick-slider">
                            <ul class="tie-slider-nav"></ul>
                            <div
                              aria-live="polite"
                              class="slick-list draggable"
                            >
                              <div
                                class="slick-track"
                                style={{ opacity: '1', width: `${dimensions.width > '1190' ? '1190' : dimensions.width - 50}px`, transform: 'translate3d(0px, 0px, 0px)' }}
                                role="listbox"
                              >
                                <div
                                  class="slide slick-slide slick-current slick-active"
                                  data-slick-index="0"
                                  aria-hidden="false"
                                  style={{ width: `${dimensions.width > '1190' ? '1190' : dimensions.width - 50}px` }}
                                  tabindex="-1"
                                  role="option"
                                  data-aria-describedby="slick-slide20"
                                >
                                  <CardOverlay />
                                  <CardOverlay />
                                  <CardOverlay />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
