import CardThree from "../../cards/card-three";
import { AiFillFile } from "react-icons/ai";
import CardFour from "../../cards/card-four";
import AdsTwo from "../../ads/ads-two";

const sectionThree = () => {
  return (
    <>
      <div
        id="tiepost-131-section-1508"
        className="section-wrapper container normal-width without-background"
      >
        <div className="section-item sidebar-right has-sidebar">
          <div className="container-normal">
            <div className="tie-row main-content-row">
              <div className="main-content col-md-8 col-xs-12" role="main">
                <div
                  id="tie-block_3075"
                  className="mag-box big-posts-box media-overlay"
                >
                  <div className="container-wrapper">
                    {/* HeadTitle */}
                    <div className="mag-box-title the-global-title">
                      <h3>Electronics</h3>
                    </div>
                    {/* sectionBody */}
                    <div className="mag-box-container clearfix">
                      <ul className="posts-items posts-list-container">
                        <CardThree />
                        <CardThree />
                        <CardThree />
                        <CardThree />
                      </ul>
                    </div>
                    {/* Show */}

                    <a
                      className="block-pagination next-posts show-more-button"
                      data-text="Show More"
                      href=""
                    >
                      Show More
                    </a>
                  </div>
                </div>
              </div>
              <aside
                className="sidebar col-md-4 col-xs-12 normal-side is-sticky is-alreay-loaded"
                style={{
                  position: "relative",
                  overflow: "visible",
                  boxSizing: "border-box",
                  minHeight: "1px",
                }}
              >
                <div
                  className="theiaStickySidebar"
                  style={{
                    paddingTop: "0px",
                    paddingBottom: "1px",
                    position: "static",
                    transform: "none",
                    top: "0px",
                    left: "928.016px",
                  }}
                >
                  <div
                    id="posts-list-widget-30"
                    className="container-wrapper widget posts-list"
                  >
                    <div class="widget-title the-global-title has-block-head-4">
                      <div class="the-subtitle">
                        <AiFillFile
                          className="widget-title-icon"
                          style={{ marginRight: "2px", alignItems: "center" }}
                        />
                        Popular Videos
                      </div>
                    </div>
                    <div className="widget-posts-list-container posts-list-counter media-overlay">
                      <ul className="posts-list-items widget-posts-wrapper">
                        <CardFour />
                        <CardFour />
                        <CardFour />
                        <CardFour />
                        <CardFour />
                      </ul>
                    </div>
                  </div>
                  {/*  */}
                  <AdsTwo />
                  {/*  */}
                </div>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default sectionThree;
