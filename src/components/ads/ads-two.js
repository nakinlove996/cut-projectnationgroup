import Slider from "react-slick";


export default function AdsTwo() {
  var settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,
  };
  return (
    <>
      <Slider {...settings}>
        <div className="slide tie-slick-slider">
          <img
            src="https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/02/maxresdefault-7-390x220.jpg"
            alt=""
          />
          <a
            href="2017/02/06/better-than-robert-redford-dessert/"
            title="Top 5 – Computer Gadgets You Must Have"
            class="all-over-thumb-link"
            tabindex="-1"
          ></a>
          <div className="thumb-overlay">
            <div className="thumb-content">
              <div class="thumb-meta">
                <div class="post-meta clearfix">
                  <span class="date meta-item tie-icon">Feb 6, 2017</span>
                </div>
              </div>
              <h3 class="thumb-title">
                <a
                  href="2017/02/06/better-than-robert-redford-dessert/"
                  title="Top 5 – Computer Gadgets You Must Have"
                  tabindex="0"
                >
                  Top 5 – Computer Gadgets You Must Have
                </a>
              </h3>
            </div>
          </div>
        </div>
{/*  */}
        <div className="slide">
          <img
            src="https://jannah.tielabs.com/videos/wp-content/uploads/sites/25/2017/01/maxresdefault-8-390x220.jpg"
            alt=""
          />
          <a
            href="2017/02/06/better-than-robert-redford-dessert/"
            title="Top 5 – Computer Gadgets You Must Have"
            class="all-over-thumb-link"
            tabindex="-1"
          ></a>
          <div className="thumb-overlay">
            <div className="thumb-content">
              <div class="thumb-meta">
                <div class="post-meta clearfix">
                  <span class="date meta-item tie-icon">Feb 6, 2017</span>
                </div>
              </div>
              <h3 class="thumb-title">
                <a
                  href="2017/02/06/better-than-robert-redford-dessert/"
                  title="Top 5 – Computer Gadgets You Must Have"
                  tabindex="0"
                >
                  Top 5 – Computer Gadgets You Must Have
                </a>
              </h3>
            </div>
          </div>
        </div>
{/*  */}

      </Slider>
    </>
  );
}
