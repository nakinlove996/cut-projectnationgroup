const adsOne = () => {
  return (
    <>
      <div
        id="tie-block_3452"
        class="mag-box stream-item-mag stream-item content-only"
      >
        <div class="container-wrapper">
          <a
            href="//tielabs.com/buy/jannah?utm_source=demos&amp;utm_campaign=jannah&amp;utm_content=videos&amp;utm_medium=ads"
            target="_blank"
          >
            <img
              src="https://jannah.tielabs.com/jannah-videos/wp-content/uploads/sites/25/2017/05/video-1-1.jpg"
              alt=""
              width="728"
              height="90"
            />
          </a>
        </div>
      </div>
    </>
  );
};
export default adsOne;
