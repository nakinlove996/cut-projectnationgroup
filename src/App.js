import "./assets/scss/style.scss";
import SectionOne from "./components/sections/section-home/section-one";
import SectionThree from "./components/sections/section-home/section-three";
import SectionTwo from "./components/sections/section-home/section-two";
import SectionFour from "./components/sections/section-home/section-four";
import SectionFive from "./components/sections/section-home/section-five";
import SectionSix from "./components/sections/section-home/section-six";

function App() {
  return (
    <>
      <SectionOne />
      <SectionTwo />
      <SectionThree />
      <SectionFour />
      <SectionFive />
      <SectionSix />
    </>
  );
}

export default App;
